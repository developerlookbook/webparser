﻿using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;
using MoyoAdParsers.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoyoAdParsers.AdListPagionationParsers
{
    public class AdListPagionationParser : HtmlParser
    {
        #region CONSTRUCTORS

        public AdListPagionationParser(HtmlDocument document) : base(document)
        {
        }

        #endregion

        #region METHODS

        public uint ParseMaxPageNumber()
        {
            var selector = @".pagination > a";
            var tags     = this.Document.QuerySelectorAll(selector);
            var numbers  = tags.Select(a => uint.TryParse(a.InnerText, out uint n) ? n : 0);

            return numbers.Max();
        }

        #endregion
    }
}
