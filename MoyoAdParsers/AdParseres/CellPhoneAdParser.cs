﻿using System;
using System.Collections.Generic;
using System.Text;
using ExtensionMethods;
using HtmlAgilityPack;

namespace MoyoAdParsers.AdParseres
{
    public class CellPhoneAdParser : PhoneAdParser
    {
        #region CONSTRUCTORS

        public CellPhoneAdParser(HtmlDocument document) : base(document)
        {
        }

        #endregion

        #region METHODS

        public override string ParseInterfaces()
        {
            var name  = @"\bИнтерфейс\s*подключения\b:";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public override string ParseScreenMatrix()
        {
            var name  = @"\bТехнология\s*дисплея\b\s*:";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public override string ParseWirelessTechnologies()
        {
            var name  = @"\bБеспроводные\s*интерфейсы\s*:";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }

        public string ParseCaseType()
        {
            var name = @"\bТип\b\s*\bкорпуса\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public int?   ParseBatteryTalkTime()
        {
            var name  = @"\bВремя\s*в\s*режиме\s*разговора[,\s]*ч[.\s]*:";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableInt();
        }
        public int?   ParseBatteryStandbyTime()
        {
            var name  = @"\bВремя\s*в\s*режиме\s*ожидания[,\s]*ч[.\s]*:";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableInt();
        }

        #endregion
    }
}
