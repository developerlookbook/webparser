﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ExtensionMethods;
using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;
using MoyoAdParsers.Shared;

namespace MoyoAdParsers.AdParseres
{
    public abstract class AdParser : HtmlParser
    {
        #region CONSTRUCTORS

        public AdParser(HtmlDocument document) : base(document)
        {
        }

        #endregion


        #region METHODS

        internal string FindValueByCharacteristicName(string pattern)
        {
            var options = RegexOptions.Multiline | RegexOptions.IgnoreCase;

            return this.Document?
                .QuerySelector("#features")?
                .QuerySelector(".full_content")?
                .QuerySelectorAll("li")?
                .FirstOrDefault(li => Regex.IsMatch(li.InnerText, pattern, options))?
                .LastChild?
                .InnerText ?? null;                           
        }

        public ulong?   ParseBarcode()
        {
            var name  = @"\bШтрихкод\b";
            var value = this.FindValueByCharacteristicName(name);
            
            return value.ToNullableUlong();
        }
        public ushort?  ParseGuarantee()
        {
            var name  = @"Гарантия";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableUshort();
        }
        public string[] ParseImageLinks()
        {
            return this.Document?
                .QuerySelector(".tovarnew-imageblock")?
                .QuerySelector(".preloadImage-container")?
                .QuerySelectorAll(".tovarnew-mainimagecontainer > a")?
                .Select(a => a.GetAttributeValue("href", null))?.ToArray();
        }
        public decimal? ParsePrice()
        {
            var value = this.Document?
                .QuerySelector(".price-block")?
                .QuerySelector("meta[itemprop=\"price\"]")?
                .GetAttributeValue("content", null);

            return value.ToNullableDecimal();
        }
        public string   ParseTitle()
        {
            return this.Document?
                .QuerySelector("h1[itemprop=\"name\"] ")?
                .InnerText;
        }

        #endregion
    }
}
