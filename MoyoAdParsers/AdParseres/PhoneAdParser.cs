﻿using System;
using System.Collections.Generic;
using System.Text;
using ExtensionMethods;
using HtmlAgilityPack;

namespace MoyoAdParsers.AdParseres
{
    public abstract class PhoneAdParser : AdParser
    {
        #region CONSTRUCTORS

        public PhoneAdParser(HtmlDocument document) : base(document)
        {
        }

        #endregion

        #region METHODS

        public string  ParseBatteryType()
        {
            var name  = @"\bАккумулятор[\s(]*тип[)\s]*:";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public int?    ParseBatteryCapacity()
        {
            var name  = @"\bЕмкость\s*аккумулятора[,\s]*мАч\s*:";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableInt();
        }        
        public string  ParseCaseMaterial()
        {
            var name  = @"\bМатериал\b\s*\bкорпуса\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public bool?   ParseCaseShockproof()
        {
            var name         = @"\bПротивоударный\b";
            var value        = this.FindValueByCharacteristicName(name)?.Trim()?.ToLower();
            var isShockproof = default(bool?);

            if (!value.IsNull())
            {
                isShockproof = !(value == "нет");
            }
            else
            {
                isShockproof = null;
            }

            return isShockproof;
        }
        public string  ParseCaseColor()
        {
            var name  = @"\bЦвет\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseCommunicationStandard()
        {
            var name  = @"\bСтандарт\b\s*\bсвязи\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseDescription()
        {
            var name  = @"\bКласс\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public double? ParseThickness()
        {
            var name  = @"\bТолщина\b";
            var value = this.FindValueByCharacteristicName(name);            

            return value.ToNullableDouble();
        }
        public double? ParseWidth()
        {
            var name  = @"\bОбщая\s*ширина\s*изделия\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableDouble();
        }
        public double? ParseHeight()
        {
            var name  = @"\bОбщая\s*высота\s*изделия\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableDouble();
        }
        public double? ParseWeight()
        {
            var name  = @"\bВес\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableDouble();
        }
        public string  ParseMadeIn()
        {
            var name  = @"\bСтрана\s*производитель\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public double? ParseScreenDiagonal()
        {
            var name  = @"\bДисплей\s*[(]*диагональ[)]*\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableDouble();
        }
        public string  ParseScreenResolution()
        {
            var name  = @"\bДисплей[\s(]*макс[\s.]*разрешение\b[\s)]*";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public ushort? ParseScreenPPI()
        {
            var name  = @"\bДисплей\s*[(]*плотность\s*пикселей\s*на\s*дюйм\b[)]*";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableUshort();
        }
        public string  ParseSIMCardFormat()
        {
            var name  = @"\bФормат\s*SIM-карты\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseSIMCardOperatingMode()
        {
            var name  = @"\bРежим\s*работы\s*SIM-карт\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public ushort? ParseSIMCardsNumber()
        {
            var name  = @"\bКол-во\s*SIM-карт\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableUshort();
        }        
        public string  ParseYearOfIssue()
        {
            var name  = @"\bГод\s*выпуска\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }

        public abstract string ParseInterfaces();
        public abstract string ParseScreenMatrix();
        public abstract string ParseWirelessTechnologies();

        #endregion
    }
}
