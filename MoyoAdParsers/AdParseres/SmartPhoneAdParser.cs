﻿using ExtensionMethods;
using HtmlAgilityPack;
using MoyoAdParsers.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MoyoAdParsers.AdParseres
{
    public class SmartPhoneAdParser : PhoneAdParser
    {
        #region CONSTRUCTORS

        public SmartPhoneAdParser(HtmlDocument document) : base(document)
        {
        }

        #endregion

        #region PROPERTIES

        public string  ParseModel()
        {
            var name  = @"\bМодель\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseDataTransmissionStandards()
        {
            var name  = @"\bСтандарты\s*передачи\s*данных\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public bool?   ParseLTE()
        {
            var name   = @"\bLTE\b";
            var value  = this.FindValueByCharacteristicName(name)?.Trim()?.ToLower();
            var hasLTE = default(bool?);

            if (!value.IsNull())
            {
                hasLTE = !(value == "нет");
            }
            else
            {
                hasLTE = null;
            }

            return hasLTE;
        }
        public double? ParseBuiltInMemory()
        {
            var name  = @"\bВстроенная\s*память\s*,\s*ГБ\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableDouble();
        }
        public double? ParseRAM()
        {
            var name  = @"\bОперативная\s*память\s*[(]*объем[),]*\s*ГБ\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableDouble();
        }
        public bool?   ParseMemoryCardSupport()
        {
            var name       = @"\bПоддержка\s*карт\s*памяти\b";
            var value      = this.FindValueByCharacteristicName(name)?.Trim()?.ToLower();
            var hasSupport = default(bool?);

            if(!value.IsNull())
            {
                hasSupport = (value == "есть");
            }
            else
            {
                hasSupport = null;
            }

            return hasSupport;
        }
        public string  ParseMaxMemoryCardCapacity()
        {
            var name  = @"\bОбъем\s*карт\s*памяти[,]*\s*до\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public ushort? ParseProcessorCoresNumber()
        {
            var name  = @"\bПроцессор[\s(]*(:?(:?к-во)|(:?количество))\s*ядер[),\s]*шт[.]*\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableUshort();
        }
        public string  ParseProcessorManufacturer()
        {
            var name  = @"\bПроцессор\s*[(]*производитель[)]*";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseProcessorModel()
        {
            var name  = @"\bПроцессор\s*[(]*модель\b[)]*";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public double? ParseProcessorClockFrequency()
        {
            var name  = @"\bПроцессор[\s(]*тактовая частота - turbo[),\s]*ГГц\b";
            var value = this.FindValueByCharacteristicName(name);           

            return value.ToNullableDouble();
        }
        public string  ParseGraphicsProcessor()
        {
            var name  = @"\bГрафический\s*процессор\s*:";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseSatelliteSystem()
        {
            var name  = @"\bСпутниковая\s*система\s*:";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public double? ParseMainCameraResolution()
        {
            var name  = @"\bОсновная\s*камера[,\s]*Мпикс\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableDouble();
        }
        public string  ParseMainCameraDescription()
        {
            var name  = @"\bОсновная\s*камера[\s\-(]*дополнительно\b[)\s]*";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseMainCameraVideoFormat()
        {
            var name  = @"\bЗапись\s*видео\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public double? ParseFrontCameraResolution()
        {
            var name  = @"\bФронтальная[\s(]*селфи[)\s]*камера[,\s]*Мпикс\b";
            var value = this.FindValueByCharacteristicName(name);

            return value.ToNullableDouble();
        }
        public string  ParseOSName()
        {
            var name  = @"\bОперационная\s*система\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseOSVersion()
        {
            var name  = @"\bВерсия\s*ОС\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseWLAN()
        {
            var name  = @"\bWLAN\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseBluetooth()
        {
            var name  = @"\bBluetooth\b\s*:";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public bool?   ParseNFCChip()
        {
            var name    = @"\bNFC[\-\s]*чип[\s(]*коммуникация\s*ближнего\s*поля[)]";
            var value   = this.FindValueByCharacteristicName(name)?.Trim()?.ToLower();
            var hasChip = default(bool?);
            
            if (!value.IsNull())
            {
                hasChip = !(value == "нет");
            }
            else
            {
                hasChip = null;
            }

            return hasChip;
        }
        public string  ParseLineupName()
        {
            var name  = @"\bМодельный\s*ряд\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public string  ParseLineupLevel()
        {
            var name  = @"\bМодельный\s*ряд\s*2\s*уровня\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public bool?   ParseScreenPositionSensor()
        {
            var name      = @"\bДатчик\s*положения\s*экрана\b";
            var value     = this.FindValueByCharacteristicName(name)?.Trim()?.ToLower();
            var hasSensor = default(bool?);

            if (!value.IsNull())
            {
                hasSensor = !(value == "нет");
            }
            else
            {
                hasSensor = null;
            }

            return hasSensor;
        }
        public bool?   ParseLightSensor()
        {
            var name      = @"\bДатчик\s*освещенности\b";
            var value     = this.FindValueByCharacteristicName(name)?.Trim()?.ToLower();
            var hasSensor = default(bool?);

            if (!value.IsNull())
            {
                hasSensor = !(value == "нет");
            }
            else
            {
                hasSensor = null;
            }

            return hasSensor;
        }
        public bool?   ParseProximitySensor()
        {
            var name      = @"\bДатчик\s*приближения\b";
            var value     = this.FindValueByCharacteristicName(name)?.Trim()?.ToLower();
            var hasSensor = default(bool?);

            if (!value.IsNull())
            {
                hasSensor = !(value == "нет");
            }
            else
            {
                hasSensor = null;
            }

            return hasSensor;
        }

        public override string ParseInterfaces()
        {
            var name  = @"\bИнтерфейсы\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public override string ParseScreenMatrix()
        {
            var name  = @"\bДисплей[\s(]*тип\s*матрицы\b[)\s]*:";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        public override string ParseWirelessTechnologies()
        {
            var name  = @"\bБеспроводные\s*технологии\b";
            var value = this.FindValueByCharacteristicName(name);

            return value;
        }
        #endregion
    }
}
