﻿using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;
using MoyoAdParsers.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoyoAdParsers.AdListParsers
{
    public class AdListParser : HtmlParser
    {
        public AdListParser(HtmlDocument document) : base(document)
        {
            this.Document = document ?? throw new ArgumentNullException(nameof(document));
        }

        public IEnumerable<Uri> ParseAdList()
        {
            var selector = @".catalog_content .productContainer > .goodsitem_inner > a.goods_title";
            var tags     = this.Document.QuerySelectorAll(selector);
            var hrefs    = tags?.Select(t => t.GetAttributeValue("href", null)) ?? null;
            var uris     = hrefs?.Where(h => h != null)?.Select(h => new Uri(this.Domain, h)) ?? null;

            return uris;
        }
    }
}
