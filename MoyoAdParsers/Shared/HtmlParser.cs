﻿using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoyoAdParsers.Shared
{
    public abstract class HtmlParser
    {
        #region FIELDS

        private HtmlDocument document;

        #endregion


        #region PROPERTIES

        protected HtmlDocument Document
        {
            get => this.document;
            set => this.document = value ?? throw new ArgumentNullException(nameof(this.Document));
        }

        protected Uri Domain => new Uri(@"https://www.moyo.ua");

        #endregion


        #region CONSTRUCTORS

        public HtmlParser(HtmlDocument document)
        {
            this.Document = document ?? throw new ArgumentNullException(nameof(document));
        }

        #endregion       
    }
}
