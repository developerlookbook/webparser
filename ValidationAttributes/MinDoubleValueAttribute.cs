﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ValidationAttributes
{
    public class MinDoubleValueAttribute : ValidationAttribute
    {
        #region PROPERTIES

        internal double MinValue { get; set; }

        #endregion

        public MinDoubleValueAttribute(double value, Func<string> errorMessageAccessor) : base(errorMessageAccessor)
        {
            this.MinValue = value;
        }

        public MinDoubleValueAttribute(double value, string errorMessage) : base(errorMessage)
        {
            this.MinValue = value;
        }

        public override bool IsValid(object value)
        {
            if (value == null     ) throw new ArgumentNullException(nameof(value));
            if (!(value is double)) throw new ArgumentException("Wrong type.", nameof(value));

            double number = (double)value;

            return (this.MinValue <= number);
        }
    }
}
