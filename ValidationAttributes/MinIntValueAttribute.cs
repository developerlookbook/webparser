﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ValidationAttributes
{
    public class MinIntValueAttribute : ValidationAttribute
    {
        #region PROPERTIES

        internal int MinValue { get; set; }

        #endregion

        #region CONSTRUCTORS

        public MinIntValueAttribute(int value, Func<string> errorMessageAccessor) : base(errorMessageAccessor)
        {
            this.MinValue = value;
        }

        public MinIntValueAttribute(int value, string errorMessage) : base(errorMessage)
        {
            this.MinValue = value;
        }

        #endregion


        #region METHODS

        public override bool IsValid(object value)
        {
            if (value == null  ) throw new ArgumentNullException(nameof(value));
            if (!(value is int)) throw new ArgumentException("Wrong type.", nameof(value));

            var number = (int)value;

            return (this.MinValue <= number);
        }

        #endregion
    }
}
