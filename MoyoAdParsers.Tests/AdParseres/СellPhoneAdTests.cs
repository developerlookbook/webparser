﻿using HtmlAgilityPack;
using MoyoAdParsers.AdParseres;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace MoyoAdParsers.Tests.AdParseres
{
    class СellPhoneAdTests
    {
        string PathToFile
        {
            get
            {
                string path;

                path = Assembly.GetExecutingAssembly().Location;
                path = Path.GetDirectoryName(path);
                path = $@"{path}\Data\CellPhone.html";

                return path;
            }
        }

        HtmlDocument HtmlDocument { get; } = new HtmlDocument();

        [SetUp]
        public void CellPhoneAdParserTestsSetUp()
        {
            this.HtmlDocument.Load(this.PathToFile);
        }

        [Test]
        public void ParseBarcodeTest()
        {
            // Arrange
            var expected = 6438158753358;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseBarcode();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseGuaranteeTest()
        {
            // Arrange
            var expected = 12;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseGuarantee();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseImageLinksTest()
        {
            // Arrange
            var expected = new String[] 
            {
                "https://img1.moyo.ua/img/products/2172/65_1500x_1500520933.jpg",
                "https://img1.moyo.ua/img/gallery/2172/65/192944_middle.jpg?1498781398",
                "https://img1.moyo.ua/img/gallery/2172/65/192945_middle.jpg?1498781398",

            };
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseImageLinks();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParsePriceTest()
        {
            // Arrange
            var expected = 1_899;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParsePrice();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseTitleTest()
        {
            // Arrange
            var expected = "Мобильный телефон Nokia 230 DS Silver White";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseTitle();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseCaseMaterialTest()
        {
            // Arrange
            var expected = "пластик/алюминий";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseCaseMaterial();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        // TODO: remove
        //[Test]
        //public void ParseCaseShockproofTest()
        //{
        //    // Arrange
        //    var expected = false;
        //    var parser   = new CellPhoneAdParser(this.HtmlDocument);

        //    // Act
        //    var actual = parser.ParseCaseShockproof();

        //    // Assert
        //    Assert.That(actual, Is.EqualTo(expected));
        //}

        [Test]
        public void ParseCaseColorTest()
        {
            // Arrange
            var expected = "белый, серебристый";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseCaseColor();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseCommunicationStandardTest()
        {
            // Arrange
            var expected = "GSM 850/1900, GSM 900/1800";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseCommunicationStandard();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseDescriptionTest()
        {
            // Arrange
            var expected = "недорогие";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseDescription();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseThicknessTest()
        {
            // Arrange
            var expected = 10.9;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseThickness();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseWidthTest()
        {
            // Arrange
            var expected = 53.4;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseWidth();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseHeightTest()
        {
            // Arrange
            var expected = 124.6;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseHeight();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseWeightTest()
        {
            // Arrange
            var expected = 91.8;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseWeight();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseInterfacesTest()
        {
            // Arrange
            var expected = "Audio-jack 3.5 mm, microUSB";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseInterfaces();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseMadeInTest()
        {
            // Arrange
            var expected = "Китай";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseMadeIn();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseScreenDiagonalTest()
        {
            // Arrange
            var expected = 2.8;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseScreenDiagonal();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }       

        [Test]
        public void ParseScreenResolutionTest()
        {
            // Arrange
            var expected = "320х240";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseScreenResolution();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseScreenMatrixTest()
        {
            // Arrange
            var expected = "TFT";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseScreenMatrix();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        // TODO: remove
        //[Test]
        //public void ParseScreenPPITest()
        //{
        //    // Arrange
        //    var expected = 282;
        //    var parser   = new CellPhoneAdParser(this.HtmlDocument);

        //    // Act
        //    var actual = parser.ParseScreenPPI();

        //    // Assert
        //    Assert.That(actual, Is.EqualTo(expected));
        //}

        [Test]
        public void ParseSIMCardFormatTest()
        {
            // Arrange
            var expected = "mini SIM";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseSIMCardFormat();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        // TODO: remove
        //[Test]
        //public void ParseSIMCardOperatingModeTest()
        //{
        //    // Arrange
        //    var expected = "попеременно";
        //    var parser   = new CellPhoneAdParser(this.HtmlDocument);

        //    // Act
        //    var actual = parser.ParseSIMCardOperatingMode();

        //    // Assert
        //    Assert.That(actual, Is.EqualTo(expected));
        //}

        [Test]
        public void ParseSIMCardsNumberTest()
        {
            // Arrange
            var expected = 2;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseSIMCardsNumber();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseWirelessTechnologiesTest()
        {
            // Arrange
            var expected = "Bluetooth 3.0";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseWirelessTechnologies();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseYearOfIssueTest()
        {
            // Arrange
            var expected = "2015";
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseYearOfIssue();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseBatteryTalkTimeTest()
        {
            // Arrange
            var expected = 23;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseBatteryTalkTime();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseBatteryStandbyTimeTest()
        {
            // Arrange
            var expected = 648;
            var parser   = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseBatteryStandbyTime();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseCaseTypeTest()
        {
            // Arrange
            var expected = "Классический (моноблок)";
            var parser = new CellPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseCaseType();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
