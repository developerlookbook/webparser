﻿using HtmlAgilityPack;
using MoyoAdParsers.AdParseres;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace MoyoAdParsers.Tests.AdParseres
{
    class SmartPhoneAdParserTests
    {
        string PathToFile
        {
            get
            {
                string path;

                path = Assembly.GetExecutingAssembly().Location;
                path = Path.GetDirectoryName(path);
                path = $@"{path}\Data\SmartPhone.html";

                return path;
            }
        }

        HtmlDocument HtmlDocument { get; } = new HtmlDocument();

        [SetUp]
        public void SmartphoneAdParserTestsSetUp()
        {
            this.HtmlDocument.Load(this.PathToFile);
        }

        [Test]
        public void ParseBarcodeTest()
        {
            // Arrange
            var expected = 8806088366203;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseBarcode();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseBatteryTypeTest()
        {
            // Arrange
            var expected = "Lithium-Ion";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseBatteryType();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseBatteryCapacityTest()
        {
            // Arrange
            var expected = 3100;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseBatteryCapacity();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseGuaranteeTest()
        {
            // Arrange
            var expected = 12;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseGuarantee();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseImageLinksTest()
        {
            // Arrange
            var expected = new String[] 
            {
                "https://img1.moyo.ua/img/products/2679/78_1500x_1500531697.jpg",
                "https://img1.moyo.ua/img/gallery/2679/78/307192_middle.jpg?1498828748",
                "https://img1.moyo.ua/img/gallery/2679/78/307190_middle.jpg?1498828748",
                "https://img1.moyo.ua/img/gallery/2679/78/307181_middle.jpg?1498828747",
                "https://img1.moyo.ua/img/gallery/2679/78/307191_middle.jpg?1498828748",
                "https://img1.moyo.ua/img/gallery/2679/78/307180_middle.jpg?1498828747",
                "https://img1.moyo.ua/img/gallery/2679/78/307182_middle.jpg?1498828747",
                "https://img1.moyo.ua/img/gallery/2679/78/307183_middle.jpg?1498828747",
                "https://img1.moyo.ua/img/gallery/2679/78/307184_middle.jpg?1498828747",
                "https://img1.moyo.ua/img/gallery/2679/78/307185_middle.jpg?1498828747"
            };
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseImageLinks();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParsePriceTest()
        {
            // Arrange
            var expected = 3_999;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParsePrice();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseTitleTest()
        {
            // Arrange
            var expected = "Samsung Galaxy J5 2016 J510H/DS Gold";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseTitle();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseCaseMaterialTest()
        {
            // Arrange
            var expected = "пластик";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseCaseMaterial();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseCaseShockproofTest()
        {
            // Arrange
            var expected = false;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseCaseShockproof();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseCaseColorTest()
        {
            // Arrange
            var expected = "золотой";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseCaseColor();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseCommunicationStandardTest()
        {
            // Arrange
            var expected = "3G, CDMA, EDGE, GPRS, GSM 850/1900, GSM 900/1800, HSDPA, WCDMA(UMTS) 900/2100";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseCommunicationStandard();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseDescriptionTest()
        {
            // Arrange
            var expected = "для девушек, для работы, с хорошей камерой";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseDescription();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseThicknessTest()
        {
            // Arrange
            var expected = 8.1d;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseThickness();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseWidthTest()
        {
            // Arrange
            var expected = 72.3d;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseWidth();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseHeightTest()
        {
            // Arrange
            var expected = 145.8d;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseHeight();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseWeightTest()
        {
            // Arrange
            var expected = 159d;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseWeight();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseInterfacesTest()
        {
            // Arrange
            var expected = "3,5 mm, microUSB";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseInterfaces();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseMadeInTest()
        {
            // Arrange
            var expected = "Китай";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseMadeIn();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseScreenDiagonalTest()
        {
            // Arrange
            var expected = 5.2;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseScreenDiagonal();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }       

        [Test]
        public void ParseScreenResolutionTest()
        {
            // Arrange
            var expected = "1280x720 (HD)";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseScreenResolution();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseScreenMatrixTest()
        {
            // Arrange
            var expected = "Super AMOLED";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseScreenMatrix();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseScreenPPITest()
        {
            // Arrange
            var expected = 282;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseScreenPPI();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseSIMCardFormatTest()
        {
            // Arrange
            var expected = "Micro-SIM";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseSIMCardFormat();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseSIMCardOperatingModeTest()
        {
            // Arrange
            var expected = "попеременно";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseSIMCardOperatingMode();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseSIMCardsNumberTest()
        {
            // Arrange
            var expected = 2;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseSIMCardsNumber();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseWirelessTechnologiesTest()
        {
            // Arrange
            var expected = "Bluetooth, GPS, Wi-Fi";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseWirelessTechnologies();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseYearOfIssueTest()
        {
            // Arrange
            var expected = "2016";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseYearOfIssue();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParserModelTest()
        {
            // Arrange
            var expected = "Galaxy J5";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseModel();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParserDataTransmissionStandardsTest()
        {
            // Arrange
            var expected = "2G, 3G";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseDataTransmissionStandards();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseLTETest()
        {
            // Arrange
            var expected = false;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseLTE();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseBuiltInMemoryTest()
        {
            // Arrange
            var expected = 16;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseBuiltInMemory();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseRAMTest()
        {
            // Arrange
            var expected = 2;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseRAM();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseMemoryCardSupportTest()
        {
            // Arrange
            var expected = true;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseMemoryCardSupport();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseMaxMemoryCardCapacityTest()
        {
            // Arrange
            var expected = "microSD до 128 Гб";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseMaxMemoryCardCapacity();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseProcessorCoresNumberTest()
        {
            // Arrange
            var expected = 4;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseProcessorCoresNumber();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseProcessorManufacturerTest()
        {
            // Arrange
            var expected = "Qualcomm Snapdragon";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseProcessorManufacturer();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseProcessorModelTest()
        {
            // Arrange
            var expected = "Snapdragon 410";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseProcessorModel();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseProcessorClockFrequencyTest()
        {
            // Arrange
            var expected = 1.2;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseProcessorClockFrequency();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseGraphicsProcessorTest()
        {
            // Arrange
            var expected = "Adreno 306";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseGraphicsProcessor();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseSatelliteSystemTest()
        {
            // Arrange
            var expected = "Glonass, GPS";
            var parser = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseSatelliteSystem();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseMainCameraResolutionTest()
        {
            // Arrange
            var expected = 13;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseMainCameraResolution();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseMainCameraDescriptionTest()
        {
            // Arrange
            var expected = "LED-вспышка, Автофокус";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseMainCameraDescription();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseMainCameraVideoFormatTest()
        {
            // Arrange
            var expected = "FHD (1920x1080)";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseMainCameraVideoFormat();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseFrontCameraResolutionTest()
        {
            // Arrange
            var expected = 5;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseFrontCameraResolution();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseOSNameTest()
        {
            // Arrange
            var expected = "Android";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseOSName();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseOSVersionTest()
        {
            // Arrange
            var expected = "Android 6.0";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseOSVersion();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseWLANTest()
        {
            // Arrange
            var expected = "802.11 a/b/g/n, Direct";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseWLAN();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseBluetoothTest()
        {
            // Arrange
            var expected = "A2DP, v4.1";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseBluetooth();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseNFCChipTest()
        {
            // Arrange
            var expected = false;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseNFCChip();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseLineupNameTest()
        {
            // Arrange
            var expected = "Galaxy";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseLineupName();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseLineupLevelTest()
        {
            // Arrange
            var expected = "J5";
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseLineupLevel();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseScreenPositionSensorTest()
        {
            // Arrange
            var expected = true;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseScreenPositionSensor();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseLightSensorTest()
        {
            // Arrange
            var expected = false;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseLightSensor();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ParseProximitySensorTest()
        {
            // Arrange
            var expected = true;
            var parser   = new SmartPhoneAdParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseProximitySensor();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
