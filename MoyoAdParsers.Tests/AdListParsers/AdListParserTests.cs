﻿using HtmlAgilityPack;
using MoyoAdParsers.AdListParsers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace MoyoAdParsers.Tests.AdListParsers
{
    class AdListParserTests
    {
        string PathToFile
        {
            get
            {
                string path;

                path = Assembly.GetExecutingAssembly().Location;
                path = Path.GetDirectoryName(path);
                path = $@"{path}\Data\SmartphoneAdList.html";

                return path;
            }
        }

        HtmlDocument HtmlDocument { get; } = new HtmlDocument();

        [SetUp]
        public void SmartphoneAdParserTestsSetUp()
        {
            this.HtmlDocument.Load(this.PathToFile);
        }

        [Test]
        public void ParseAdListTest()
        {
            // Arrange
            var expected = new Uri[]
            {
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_j5_2016_j510h_ds_gold/267978.html"),
                new Uri("https://www.moyo.ua/smartfon-samsung-galaxy-j3-j320f-black/200597.html"),
                new Uri("https://www.moyo.ua/smartfon_huawei_p8_lite_2017_black/325054.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_j7_2017_j730f_black/356771.html"),
                new Uri("https://www.moyo.ua/apple_iphone_7_32_gb_black/279754.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_a7_2017/286676.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_j5_2017_j530f_black/356688.html"),
                new Uri("https://www.moyo.ua/smartfon-apple-iphone-7-plus-32-gb_black/195478.html"),
                new Uri("https://www.moyo.ua/smartfon-samsung-galaxy-j1-2016-j120h-gold/201679.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_j7_2017_j730f_gold/356723.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_a3_2017_ds_a320f_gold/314063.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_j5_2017_j530f_gold/356702.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_j5_2017_j530f_silver/356763.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_j7_2017_j730f_silver/356770.html"),
                new Uri("https://www.moyo.ua/smartfon_samsung_galaxy_a7_2017_ds_a720f_gold/314070.html"),
                new Uri("https://www.moyo.ua/smartfon_huawei_p8_lite_2017_gold/324658.html"),
                new Uri("https://www.moyo.ua/smartfon_apple_iphone_6s_32gb_rose_gold/286865.html"),
                new Uri("https://www.moyo.ua/apple_iphone_7_128_gb_black/280229.html"),
                new Uri("https://www.moyo.ua/smartfon_apple_iphone_6s_32gb_silver/286979.html"),
                new Uri("https://www.moyo.ua/smartfon_apple_iphone_6s_32gb_gold/287002.html"),
                new Uri("https://www.moyo.ua/smartfon_apple_iphone_6s_cpo_16gb_space_gray/377208.html"),
            };

            var parser = new AdListParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseAdList();

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
