﻿using HtmlAgilityPack;
using MoyoAdParsers.AdListPagionationParsers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace MoyoAdParsers.Tests.AdListPagionationParsers
{
    class AdListPagionationParseTests
    {
        string PathToFile
        {
            get
            {
                string path;

                path = Assembly.GetExecutingAssembly().Location;
                path = Path.GetDirectoryName(path);
                path = $@"{path}\Data\SmartphoneAdList.html";

                return path;
            }
        }

        HtmlDocument HtmlDocument { get; } = new HtmlDocument();

        [SetUp]
        public void SmartphoneAdParserTestsSetUp()
        {
            this.HtmlDocument.Load(this.PathToFile);
        }

        [Test]
        public void ParseMaxPageNumber()
        {
            // Arrange
            var excpect = 84;
            var parser = new AdListPagionationParser(this.HtmlDocument);

            // Act
            var actual = parser.ParseMaxPageNumber();

            // Assert
            Assert.That(actual, Is.EqualTo(excpect));
        }
    }
}
