﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServerApp.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CellPhoneCase",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Material = table.Column<string>(nullable: true),
                    Color = table.Column<string>(nullable: true),
                    IsShockproof = table.Column<bool>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CellPhoneCase", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CellPhoneScreen",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Resolution = table.Column<string>(nullable: true),
                    Matrix = table.Column<string>(nullable: true),
                    Diagonal = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CellPhoneScreen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Dimension",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Thickness = table.Column<double>(nullable: true),
                    Width = table.Column<double>(nullable: true),
                    Height = table.Column<double>(nullable: true),
                    Weight = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dimension", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OS",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OS", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhoneCamera",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Resolution = table.Column<double>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    VideoFormat = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoneCamera", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhoneLineup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Level = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoneLineup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Processor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CoresNumber = table.Column<int>(nullable: true),
                    Manufacturer = table.Column<string>(nullable: true),
                    Model = table.Column<string>(nullable: true),
                    ClockFrequency = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Processor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SIMCard",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Format = table.Column<string>(nullable: true),
                    OperatingMode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SIMCard", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SmartPhoneCase",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Material = table.Column<string>(nullable: true),
                    Color = table.Column<string>(nullable: true),
                    IsShockproof = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmartPhoneCase", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SmartPhoneScreen",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Resolution = table.Column<string>(nullable: true),
                    Matrix = table.Column<string>(nullable: true),
                    Diagonal = table.Column<double>(nullable: true),
                    PPI = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmartPhoneScreen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WebSites",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Url = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebSites", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CellPhoneAds",
                columns: table => new
                {
                    Title = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: true),
                    Guarantee = table.Column<int>(nullable: true),
                    Barcode = table.Column<decimal>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CommunicationStandard = table.Column<string>(nullable: true),
                    MadeIn = table.Column<string>(nullable: true),
                    YearOfIssue = table.Column<string>(nullable: true),
                    WirelessTechnologies = table.Column<string>(nullable: true),
                    Interfaces = table.Column<string>(nullable: true),
                    SIMCardsNumber = table.Column<int>(nullable: true),
                    SIMCardId = table.Column<int>(nullable: true),
                    DimensionId = table.Column<int>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CaseId = table.Column<int>(nullable: true),
                    ScreenId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CellPhoneAds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CellPhoneAds_CellPhoneCase_CaseId",
                        column: x => x.CaseId,
                        principalTable: "CellPhoneCase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CellPhoneAds_Dimension_DimensionId",
                        column: x => x.DimensionId,
                        principalTable: "Dimension",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CellPhoneAds_SIMCard_SIMCardId",
                        column: x => x.SIMCardId,
                        principalTable: "SIMCard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CellPhoneAds_CellPhoneScreen_ScreenId",
                        column: x => x.ScreenId,
                        principalTable: "CellPhoneScreen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SmartPhoneAds",
                columns: table => new
                {
                    Title = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: true),
                    Guarantee = table.Column<int>(nullable: true),
                    Barcode = table.Column<decimal>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CommunicationStandard = table.Column<string>(nullable: true),
                    MadeIn = table.Column<string>(nullable: true),
                    YearOfIssue = table.Column<string>(nullable: true),
                    WirelessTechnologies = table.Column<string>(nullable: true),
                    Interfaces = table.Column<string>(nullable: true),
                    SIMCardsNumber = table.Column<int>(nullable: true),
                    SIMCardId = table.Column<int>(nullable: true),
                    DimensionId = table.Column<int>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Model = table.Column<string>(nullable: true),
                    DataTransmissionStandards = table.Column<string>(nullable: true),
                    GraphicsProcessor = table.Column<string>(nullable: true),
                    SatelliteSystem = table.Column<string>(nullable: true),
                    HasLTE = table.Column<bool>(nullable: true),
                    BuiltInMemory = table.Column<double>(nullable: true),
                    RAM = table.Column<double>(nullable: true),
                    HasMemoryCardSupport = table.Column<bool>(nullable: true),
                    MaxMemoryCardCapacity = table.Column<string>(nullable: true),
                    ProcessorId = table.Column<int>(nullable: true),
                    MainCameraId = table.Column<int>(nullable: true),
                    FrontCameraId = table.Column<int>(nullable: true),
                    OSId = table.Column<int>(nullable: true),
                    WLAN = table.Column<string>(nullable: true),
                    Bluetooth = table.Column<string>(nullable: true),
                    HasNFCChip = table.Column<bool>(nullable: true),
                    LineupId = table.Column<int>(nullable: true),
                    HasScreenPositionSensor = table.Column<bool>(nullable: true),
                    HasLightSensor = table.Column<bool>(nullable: true),
                    HasProximitySensor = table.Column<bool>(nullable: true),
                    CaseId = table.Column<int>(nullable: true),
                    ScreenId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmartPhoneAds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_SmartPhoneCase_CaseId",
                        column: x => x.CaseId,
                        principalTable: "SmartPhoneCase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_Dimension_DimensionId",
                        column: x => x.DimensionId,
                        principalTable: "Dimension",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_PhoneCamera_FrontCameraId",
                        column: x => x.FrontCameraId,
                        principalTable: "PhoneCamera",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_PhoneLineup_LineupId",
                        column: x => x.LineupId,
                        principalTable: "PhoneLineup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_PhoneCamera_MainCameraId",
                        column: x => x.MainCameraId,
                        principalTable: "PhoneCamera",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_OS_OSId",
                        column: x => x.OSId,
                        principalTable: "OS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_Processor_ProcessorId",
                        column: x => x.ProcessorId,
                        principalTable: "Processor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_SIMCard_SIMCardId",
                        column: x => x.SIMCardId,
                        principalTable: "SIMCard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SmartPhoneAds_SmartPhoneScreen_ScreenId",
                        column: x => x.ScreenId,
                        principalTable: "SmartPhoneScreen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Photo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Image = table.Column<byte[]>(maxLength: 2000000000, nullable: false),
                    CellPhoneAdId = table.Column<int>(nullable: true),
                    SmartPhoneAdId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Photo_CellPhoneAds_CellPhoneAdId",
                        column: x => x.CellPhoneAdId,
                        principalTable: "CellPhoneAds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Photo_SmartPhoneAds_SmartPhoneAdId",
                        column: x => x.SmartPhoneAdId,
                        principalTable: "SmartPhoneAds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CellPhoneAds_CaseId",
                table: "CellPhoneAds",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_CellPhoneAds_DimensionId",
                table: "CellPhoneAds",
                column: "DimensionId");

            migrationBuilder.CreateIndex(
                name: "IX_CellPhoneAds_SIMCardId",
                table: "CellPhoneAds",
                column: "SIMCardId");

            migrationBuilder.CreateIndex(
                name: "IX_CellPhoneAds_ScreenId",
                table: "CellPhoneAds",
                column: "ScreenId");

            migrationBuilder.CreateIndex(
                name: "IX_Photo_CellPhoneAdId",
                table: "Photo",
                column: "CellPhoneAdId");

            migrationBuilder.CreateIndex(
                name: "IX_Photo_SmartPhoneAdId",
                table: "Photo",
                column: "SmartPhoneAdId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_CaseId",
                table: "SmartPhoneAds",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_DimensionId",
                table: "SmartPhoneAds",
                column: "DimensionId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_FrontCameraId",
                table: "SmartPhoneAds",
                column: "FrontCameraId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_LineupId",
                table: "SmartPhoneAds",
                column: "LineupId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_MainCameraId",
                table: "SmartPhoneAds",
                column: "MainCameraId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_OSId",
                table: "SmartPhoneAds",
                column: "OSId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_ProcessorId",
                table: "SmartPhoneAds",
                column: "ProcessorId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_SIMCardId",
                table: "SmartPhoneAds",
                column: "SIMCardId");

            migrationBuilder.CreateIndex(
                name: "IX_SmartPhoneAds_ScreenId",
                table: "SmartPhoneAds",
                column: "ScreenId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Photo");

            migrationBuilder.DropTable(
                name: "WebSites");

            migrationBuilder.DropTable(
                name: "CellPhoneAds");

            migrationBuilder.DropTable(
                name: "SmartPhoneAds");

            migrationBuilder.DropTable(
                name: "CellPhoneCase");

            migrationBuilder.DropTable(
                name: "CellPhoneScreen");

            migrationBuilder.DropTable(
                name: "SmartPhoneCase");

            migrationBuilder.DropTable(
                name: "Dimension");

            migrationBuilder.DropTable(
                name: "PhoneCamera");

            migrationBuilder.DropTable(
                name: "PhoneLineup");

            migrationBuilder.DropTable(
                name: "OS");

            migrationBuilder.DropTable(
                name: "Processor");

            migrationBuilder.DropTable(
                name: "SIMCard");

            migrationBuilder.DropTable(
                name: "SmartPhoneScreen");
        }
    }
}
