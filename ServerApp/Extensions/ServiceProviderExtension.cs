﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApp.Extensions
{
    public static class ServiceProviderExtension
    {
        public static void AddSqlServer(this IServiceCollection services, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString("WebParserDatabase");

            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(connection, b => b.MigrationsAssembly("ServerApp"));
            });
        }
    }
}
