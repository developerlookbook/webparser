﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Dimensions
{
    public class Dimension
    {
        #region FIELDS

        private int _id;

        #endregion

        #region PROPERTIES

        public int     Id        { get => this._id; private set => this._id = value; }
        public double? Thickness { get; set; }
        public double? Width     { get; set; }
        public double? Height    { get; set; }
        public double? Weight    { get; set; }

        #endregion
    }
}
