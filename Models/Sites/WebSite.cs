﻿using Models.Ads;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Sites
{
    public class WebSite
    {
        #region FIELDS

        private int _id;

        #endregion

        #region PROPERTIES
        
        public int         Id                { get => this.Id; private set => this._id = value; }
        public string      Url               { get; set; }
        public string      Name              { get; set; }
        List<SmartPhoneAd> SmartPhoneAds     { get; set; }
        List<CellPhoneAd>  CellPhoneAds      { get; set; }

        #endregion

        #region CONSTRUCTORS

        public WebSite(string url, string name)
        {
            this.Url  = url  ?? throw new ArgumentNullException(nameof(url ));
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        #endregion
    }
}
