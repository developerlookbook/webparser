﻿using ExtensionMethods;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Models.Ads;
using Models.Sites;
using System;

namespace Models
{
    public class AppDbContext : DbContext
    {
        DbSet<WebSite>      WebSites      { get; set; }
        DbSet<SmartPhoneAd> SmartPhoneAds { get; set; }
        DbSet<CellPhoneAd>  CellPhoneAds  { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }        
    }
}
