﻿using Models.Telecommunications.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Cases
{
    public class CellPhoneCase : PhoneCase
    {
        #region FIELDS

        private string _type;

        #endregion

        #region PROPERTIES

        public string Type { get => this._type; set => this._type = value?.Trim(); }

        #endregion
    }
}
