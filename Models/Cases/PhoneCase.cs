﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Cases
{
    public abstract class PhoneCase : Case
    {
        #region FIELDS

        private string _caseMaterial;
        private string _color;

        #endregion

        #region PROPERTIES

        public string Material     { get => this._caseMaterial; set => this._caseMaterial = value?.Trim(); }
        public string Color        { get => this._color;        set => this._color = value?.Trim();        }
        public bool?  IsShockproof { get; set; }

        #endregion
    }
}
