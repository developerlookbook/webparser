﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Processors
{
    public class Processor
    {
        #region FIELDS

        private int _id;

        #endregion

        #region PROPERTIES

        public int     Id             { get => this._id; private set => this._id = value; }
        public ushort? CoresNumber    { get; set; }
        public string  Manufacturer   { get; set; }
        public string  Model          { get; set; }
        public double? ClockFrequency { get; set; }

        #endregion
    }
}
