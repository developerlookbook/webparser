﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Telecommunications.Shared
{
    public class SIMCard
    {
        #region FIELDS

        private int    _id;
        private string _format;
        private string _operatingMode;

        #endregion

        #region PROPERTIES

        public int    Id            { get => this._id;    private set => this._id = value;                    }
        public string Format        { get => this._format;        set => this._format = value?.Trim();        }
        public string OperatingMode { get => this._operatingMode; set => this._operatingMode = value?.Trim(); }

        #endregion
    }
}
