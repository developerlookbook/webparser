﻿using Models.Cameras;
using Models.Cases;
using Models.Lineups;
using Models.Processors;
using Models.Screens;
using Models.Telecommunications;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Ads
{
    public class SmartPhoneAd : PhoneAd<SmartPhoneCase, SmartPhoneScreen>
    {
        #region FIELDS

        private int    _id;
        private string _model;
        private string _dataTransmissionStandards;
        private string _lte;
        private string _graphicsProcessor;
        private string _satelliteSystem;

        #endregion

        #region PROPERTIES

        public override int Id                        { get => this._id;             protected set => this._id = value; }

        public string       Model                     { get => this._model;                     set => this._model = value?.Trim();                      }
        public string       DataTransmissionStandards { get => this._dataTransmissionStandards; set => this._dataTransmissionStandards = value?.Trim(); }  
        public string       GraphicsProcessor         { get => this._graphicsProcessor;         set => this._graphicsProcessor = value?.Trim();         }     
        public string       SatelliteSystem           { get => this._satelliteSystem;           set => this._satelliteSystem = value?.Trim();           }   
                            
        public bool?        HasLTE                    { get; set; }
        public double?      BuiltInMemory             { get; set; }
        public double?      RAM                       { get; set; }
        public bool?        HasMemoryCardSupport      { get; set; }
        public string       MaxMemoryCardCapacity     { get; set; }                                                                                                  
        public Processor    Processor                 { get; set; }
        public PhoneCamera  MainCamera                { get; set; }
        public PhoneCamera  FrontCamera               { get; set; }
        public OS           OS                        { get; set; }
        public string       WLAN                      { get; set; }
        public string       Bluetooth                 { get; set; }
        public bool?        HasNFCChip                { get; set; }
        public PhoneLineup  Lineup                    { get; set; }
        public bool?        HasScreenPositionSensor   { get; set; }
        public bool?        HasLightSensor            { get; set; }
        public bool?        HasProximitySensor        { get; set; }

        public override SmartPhoneCase   Case   { get; set; }
        public override SmartPhoneScreen Screen { get; set; }

        #endregion
    }
}
