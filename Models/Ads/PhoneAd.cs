﻿using Models.Cases;
using Models.Dimensions;
using Models.Screens;
using Models.Telecommunications;
using Models.Telecommunications.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Ads
{
    [NotMapped]
    public abstract class PhoneAd<TPhoneCase, TPhoneScreen> : Ad 
        where TPhoneCase   : PhoneCase
        where TPhoneScreen : PhoneScreen
    {
        #region FIELDS

        private string _description;
        private string _communicationStandard;
        private string _madeIn; 
        private string _yearOfIssue;
        private string _wirelessTechnologies;
        private string _interfaces;

        #endregion

        #region PROPERTIES

        public abstract int Id { get; protected set; } 

        public string    Description           { get => this._description;           set => this._description = value?.Trim();           }
        public string    CommunicationStandard { get => this._communicationStandard; set => this._communicationStandard = value?.Trim(); }
        public string    MadeIn                { get => this._madeIn;                set => this._madeIn = value?.Trim();                }
        public string    YearOfIssue           { get => this._yearOfIssue;           set => this._yearOfIssue = value?.Trim();           }
        public string    WirelessTechnologies  { get => this._wirelessTechnologies;  set => this._wirelessTechnologies = value?.Trim();  }
        public string    Interfaces            { get => this._interfaces;            set => this._interfaces = value?.Trim();            }
        public ushort?   SIMCardsNumber        { get; set; }
        public SIMCard   SIMCard               { get; set; }        
        public Dimension Dimension             { get; set; }

        public abstract TPhoneCase   Case   { get; set; }
        public abstract TPhoneScreen Screen { get; set; }

        #endregion
    }
}
