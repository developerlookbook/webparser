﻿using Models.Photos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Ads
{
    [NotMapped]
    public abstract class Ad
    {
        #region FIELDS

        private List<Photo> _photos;
        private string      _title;

        #endregion

        #region PROPERTIES

        public string      Title
        {
            get => this._title;
            set => this._title = value?.Trim() ?? throw new ArgumentNullException(nameof(this.Title));
        }
        public decimal?    Price     { get; set; }          
        public List<Photo> Photos    { get; set; }
        public ushort?     Guarantee { get; set; }
        public ulong?      Barcode   { get; set; }

        #endregion
    }
}
