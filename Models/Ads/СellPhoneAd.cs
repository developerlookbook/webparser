﻿using Models.Cases;
using Models.Screens;
using Models.Telecommunications;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Ads
{

    public class CellPhoneAd : PhoneAd<CellPhoneCase, CellPhoneScreen>
    {
        #region FIELDS

        private int _id;

        #endregion

        #region PROPERTIES

        public override int             Id     { get => this._id; protected set => this._id = value; }
        public override CellPhoneCase   Case   { get; set; }
        public override CellPhoneScreen Screen { get; set; }

        #endregion
    }
}
