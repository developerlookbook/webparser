﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Batteries
{
    public class CellPhoneBattery : Battery
    {
        public int? TalkTime    { get; set; }
        public int? StandbyTime { get; set; }
    }
}
