﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Batteries
{
    public abstract class PhoneBattery : Battery
    {
        #region FIELDS

        private string _type;

        #endregion

        #region PROPERTIES

        public string Type     { get => this._type; set => this._type = value?.Trim(); }
        public int?   Capacity { get; set; }

        #endregion
    }
}
