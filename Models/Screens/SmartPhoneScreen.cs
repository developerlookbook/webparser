﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Screens
{
    public class SmartPhoneScreen : PhoneScreen
    {
        #region PROPERTIES

        public ushort? PPI { get; set; }

        #endregion
    }
}
