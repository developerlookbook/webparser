﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Screens
{
    public abstract class PhoneScreen : Screen
    {
        #region FIELDS

        private string _resolution;
        private string _matrix;

        #endregion

        #region PROPERTIES

        public string  Resolution { get => this._resolution; set => this._resolution = value?.Trim(); }
        public string  Matrix     { get => this._matrix;     set => this._matrix = value?.Trim();     }
        public double? Diagonal   { get; set; }

        #endregion
    }
}
