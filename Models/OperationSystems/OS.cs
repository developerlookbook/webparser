﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Telecommunications
{
    public class OS
    {
        #region FIELD

        private int    _id;
        private string _name;
        private string _version;
        #endregion

        #region PROPERTIES

        public int    Id      { get => this._id; private set => this._id = value; }

        public string Name    { get => this._name;    set => this._name    = value?.Trim(); }
        public string Version { get => this._version; set => this._version = value?.Trim(); }

        #endregion
    }
}
