﻿using ExtensionMethods;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Photos
{
    public class Photo
    {
        #region FIELDS

        private int    _id;
        private byte[] _image;

        #endregion

        #region PROPERTIES

        public int    Id
        {
                    get => this._id;
            private set => this._id = value;
        }        

        [Required]
        [MaxLength(2_000_000_000)]
        public byte[] Image
        {
            get => this._image;
            set
            {
                value.IfNull((v) => throw new ArgumentNullException(nameof(this.Image)));
                Photo.IsOutOfMaxLength(value,(v) => throw new ArgumentException("Size of the photo is out of max size limit.", nameof(this.Image)));

                this._image = value;
            }
        }

        [NotMapped]
        public static int MaxLength { get; } = 2_000_000_000;

        #endregion

        #region METHODS

        public static bool IsOutOfMaxLength(byte[] image)
        {
            return (MaxLength < image.Length);
        }
        public static void IsOutOfMaxLength(byte[] image, Action<byte[]> action)
        {
            if (IsOutOfMaxLength(image)) { action(image); }
        }

        #endregion
    }
}
