﻿using Models.AdBuilders;
using Models.Ads;
using Models.Cases;
using Models.Screens;
using MoyoAdParsers.AdParseres;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.AdDirectors
{
    [NotMapped]
    public class AdBuildDirector<TAd> where TAd : Ad
    {
        #region PROPERTIES

        private IAdBuilder<TAd> AdBuilder { get; }

        #endregion

        #region CONSTRUCTORS

        public AdBuildDirector(IAdBuilder<TAd> builder)
        {
            this.AdBuilder = builder ?? throw new ArgumentNullException(nameof(builder));
        }

        #endregion

        #region METHODS        

        public  TAd          Construct()
        {
            TAd ad;

            switch (this.AdBuilder)
            {
                case CellPhoneAdBuilder  builder:
                    
                    ad = this.Construct(builder) as TAd;
                    break;

                case SmartPhoneAdBuilder builder:

                    ad = this.Construct(builder) as TAd;
                    break;

                default:
                    throw new ArgumentException();
            }

            return ad;
        }
        private CellPhoneAd  Construct(CellPhoneAdBuilder  builder)
        {
            builder.SetBarcode();
            builder.SetCase();
            builder.SetCommunicationStandard();
            builder.SetDescription();
            builder.SetDimension();
            builder.SetGuarantee();
            builder.SetInterfaces();
            builder.SetMadeIn();
            builder.SetPhotos();
            builder.SetPrice();
            builder.SetScreen();
            builder.SetSIMCard();
            builder.SetSIMCardsNumber();
            builder.SetTitle();
            builder.SetWirelessTechnologies();
            builder.SetYearOfIssue();

            return builder.Build();
        }
        private SmartPhoneAd Construct(SmartPhoneAdBuilder builder)
        {
            builder.SetBarcode();
            builder.SetBluetooth();
            builder.SetBuiltInMemory();
            builder.SetCase();
            builder.SetCommunicationStandard();
            builder.SetDataTransmissionStandards();
            builder.SetDescription();
            builder.SetDimension();
            builder.SetFrontCamera();
            builder.SetGraphicsProcessor();
            builder.SetGuarantee();
            builder.SetInterfaces();
            builder.SetLightSensor();
            builder.SetLineup();
            builder.SetLTE();
            builder.SetMadeIn();
            builder.SetMainCamera();
            builder.SetMaxMemoryCardCapacity();
            builder.SetMemoryCardSupport();
            builder.SetModel();
            builder.SetNFCChip();
            builder.SetOS();
            builder.SetPhotos();
            builder.SetPrice();
            builder.SetProcessor();
            builder.SetProximitySensor();
            builder.SetRAM();
            builder.SetSatelliteSystem();
            builder.SetScreen();
            builder.SetScreenPositionSensor();
            builder.SetSIMCard();
            builder.SetSIMCardsNumber();
            builder.SetTitle();
            builder.SetWirelessTechnologies();
            builder.SetWLAN();
            builder.SetYearOfIssue();

            return builder.Build();
        }
        

        #endregion
    }
}
