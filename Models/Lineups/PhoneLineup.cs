﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Lineups
{
    public class PhoneLineup : Lineup
    {
        #region FIELDS

        private string _lineup;
        private string _lineupLevel;

        #endregion

        #region PROPERTIES

        public string Name  { get => this._lineup;      set => this._lineup = value?.Trim();      }
        public string Level { get => this._lineupLevel; set => this._lineupLevel = value?.Trim(); }

        #endregion
    }
}
