﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Lineups
{
    public abstract class Lineup
    {
        #region FIELDS

        private int _id;

        #endregion

        #region PROPERTIES

        public int Id { get => this._id; private set => this._id = value; }

        #endregion
    }
}
