﻿using Models.Ads;
using Models.Cases;
using Models.Screens;
using MoyoAdParsers.AdParseres;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.AdBuilders
{
    [NotMapped]
    public class CellPhoneAdBuilder : PhoneAdBuilder<CellPhoneAd, CellPhoneCase, CellPhoneScreen, CellPhoneAdParser, CellPhoneCase, CellPhoneScreen>
    {
        #region PROPERTIES

        protected override CellPhoneAd     Ad     { get; } = new CellPhoneAd();
        protected override CellPhoneCase   Case   { get; } = new CellPhoneCase();
        protected override CellPhoneScreen Screen { get; } = new CellPhoneScreen();
        #endregion


        #region CONSTRUCTORS

        public CellPhoneAdBuilder(CellPhoneAdParser parser) : base(parser)
        {
        }

        #endregion


        #region METHODS

        public override void SetCase()
        {
            this.Ad.Case              = this.Case;
            this.Ad.Case.Color        = this.Parser.ParseCaseColor();
            this.Ad.Case.IsShockproof = this.Parser.ParseCaseShockproof();
            this.Ad.Case.Material     = this.Parser.ParseCaseMaterial();
            this.Ad.Case.Type         = this.Parser.ParseCaseType();            
        }
        public override void SetScreen()
        {
            this.Ad.Screen            = this.Screen;
            this.Ad.Screen.Diagonal   = this.Parser.ParseScreenDiagonal();
            this.Ad.Screen.Resolution = this.Parser.ParseScreenResolution();
            this.Ad.Screen.Matrix     = this.Parser.ParseScreenMatrix();
        }        

        #endregion
    }
}
