﻿using Models.Ads;
using Models.Cases;
using Models.Dimensions;
using Models.Photos;
using Models.Screens;
using MoyoAdParsers.AdParseres;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using System.Text;

namespace Models.AdBuilders
{
    [NotMapped]
    public abstract class AdBuilder<TAd, TCase, TScreen, TAdParser> : IAdBuilder<TAd>
        where TAd       : Ad
        where TCase     : Case
        where TScreen   : Screen
        where TAdParser : AdParser
    {
        #region PROPERTIES

        protected abstract TAd     Ad     { get; }
        protected abstract TCase   Case   { get; }
        protected abstract TScreen Screen { get; }

        protected Dimension Dimension { get; } = new Dimension();
        protected TAdParser Parser    { get; }

        #endregion


        #region CONSTRUCTORS

        protected AdBuilder(TAdParser parser)
        {
            this.Parser = parser ?? throw new ArgumentNullException(nameof(parser));
        }

        #endregion


        #region METHODS

        public void SetTitle()
        {
            this.Ad.Title = this.Parser.ParseTitle();
        }
        public void SetPrice()
        {
            this.Ad.Price = this.Parser.ParsePrice();
        }
        public void SetPhotos()
        {           
            using (WebClient client = new WebClient())
            {
                var photos = new List<Photo>(10);
                var links  = this.Parser.ParseImageLinks();

                foreach (var link in links)
                {
                    if (link == null) { continue; }

                    photos.Add(new Photo() { Image = client.DownloadData(new Uri(link)) });
                }

                this.Ad.Photos = photos;
            }                
        }
        public void SetGuarantee()
        {
            this.Ad.Guarantee = this.Parser.ParseGuarantee();
        }        
        public void SetBarcode()
        {
            this.Ad.Barcode = this.Parser.ParseBarcode();
        }

        public TAd  Build()
        {
            return this.Ad;
        }

        #endregion

    }
}
