﻿using Models.Ads;

namespace Models.AdBuilders
{
    public interface IAdBuilder<TAd> where TAd : Ad
    {
        TAd Build();
    }
}