﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Models.Ads;
using Models.Cases;
using Models.Dimensions;
using Models.Screens;
using Models.Telecommunications.Shared;
using MoyoAdParsers.AdParseres;

namespace Models.AdBuilders
{
    [NotMapped]
    public abstract class PhoneAdBuilder<TAd, TCase, TScreen, TAdParser, TPhoneCase, TPhoneScreen> 
        : AdBuilder<TAd, TCase, TScreen, TAdParser>

        where TAd          : PhoneAd<TPhoneCase, TPhoneScreen>
        where TCase        : PhoneCase
        where TScreen      : PhoneScreen
        where TAdParser    : PhoneAdParser
        where TPhoneCase   : PhoneCase
        where TPhoneScreen : PhoneScreen
    {
        #region CONSTRUCTORA

        protected PhoneAdBuilder(TAdParser parser) : base(parser)
        {
        }

        #endregion

        #region METHODS

        public void SetDescription()
        {
            this.Ad.Description = this.Parser.ParseDescription();
        }
        public void SetCommunicationStandard()
        {
            this.Ad.CommunicationStandard = this.Parser.ParseCommunicationStandard();
        }
        public void SetMadeIn()
        {
            this.Ad.MadeIn = this.Parser.ParseMadeIn();
        }
        public void SetYearOfIssue()
        {
            this.Ad.YearOfIssue = this.Parser.ParseYearOfIssue();
        }
        public void SetWirelessTechnologies()
        {
            this.Ad.WirelessTechnologies = this.Parser.ParseWirelessTechnologies();
        }
        public void SetInterfaces()
        {
            this.Ad.Interfaces = this.Parser.ParseInterfaces();
        }
        public void SetSIMCardsNumber()
        {
            this.Ad.SIMCardsNumber = this.Parser.ParseSIMCardsNumber();
        }        
        public void SetSIMCard()
        {
            this.Ad.SIMCard = new SIMCard
            {
                Format        = this.Parser.ParseSIMCardFormat(),
                OperatingMode = this.Parser.ParseSIMCardOperatingMode(),
            };
        }        
        public void SetDimension()
        {
            this.Ad.Dimension           = this.Dimension;
            this.Ad.Dimension.Height    = this.Parser.ParseHeight();
            this.Ad.Dimension.Width     = this.Parser.ParseWidth();
            this.Ad.Dimension.Thickness = this.Parser.ParseThickness();
            this.Ad.Dimension.Weight    = this.Parser.ParseWeight();
           
        }

        public abstract void SetCase();
        public abstract void SetScreen();

        #endregion
    }
}
