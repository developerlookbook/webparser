﻿using Models.Ads;
using Models.Cameras;
using Models.Cases;
using Models.Lineups;
using Models.Processors;
using Models.Screens;
using Models.Telecommunications;
using MoyoAdParsers.AdParseres;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.AdBuilders
{
    [NotMapped]
    public class SmartPhoneAdBuilder : PhoneAdBuilder<SmartPhoneAd, SmartPhoneCase, SmartPhoneScreen, SmartPhoneAdParser, SmartPhoneCase, SmartPhoneScreen>
    {
        #region PROPERTIES

        protected override SmartPhoneAd     Ad     { get; } = new SmartPhoneAd();
        protected override SmartPhoneCase   Case   { get; } = new SmartPhoneCase();
        protected override SmartPhoneScreen Screen { get; } = new SmartPhoneScreen();

        #endregion

        #region CONSTRUCTORS

        public SmartPhoneAdBuilder(SmartPhoneAdParser parser) : base(parser)
        {
        }

        #endregion

        #region METHODS

        public void SetModel()
        {
            this.Ad.Model = this.Parser.ParseModel();
        }
        public void SetDataTransmissionStandards()
        {
            this.Ad.DataTransmissionStandards = this.Parser.ParseDataTransmissionStandards();
        }
        public void SetGraphicsProcessor()
        {
            this.Ad.GraphicsProcessor = this.Parser.ParseGraphicsProcessor();
        }
        public void SetSatelliteSystem()
        {
            this.Ad.SatelliteSystem = this.Parser.ParseSatelliteSystem();
        }
        public void SetLTE()
        {
            this.Ad.HasLTE = this.Parser.ParseLTE();
        }
        public void SetBuiltInMemory()
        {
            this.Ad.BuiltInMemory = this.Parser.ParseBuiltInMemory();
        }
        public void SetRAM()
        {
            this.Ad.RAM = this.Parser.ParseRAM();
        }
        public void SetMemoryCardSupport()
        {
            this.Ad.HasMemoryCardSupport = this.Parser.ParseMemoryCardSupport();
        }
        public void SetMaxMemoryCardCapacity()
        {
            this.Ad.MaxMemoryCardCapacity = this.Parser.ParseMaxMemoryCardCapacity();
        }
        public void SetProcessor()
        {
            this.Ad.Processor = new Processor()
            {
                ClockFrequency = this.Parser.ParseProcessorClockFrequency(),
                CoresNumber    = this.Parser.ParseProcessorCoresNumber(),
                Manufacturer   = this.Parser.ParseProcessorManufacturer(),
                Model          = this.Parser.ParseProcessorModel()
            };
        }
        public void SetMainCamera()
        {
            this.Ad.MainCamera = new PhoneCamera()
            {
                Description = this.Parser.ParseMainCameraDescription(),
                Resolution  = this.Parser.ParseMainCameraResolution(),
                VideoFormat = this.Parser.ParseMainCameraVideoFormat(),
            };
        }
        public void SetFrontCamera()
        {
            this.Ad.FrontCamera = new PhoneCamera()
            {
                Description = null,
                Resolution  = this.Parser.ParseFrontCameraResolution(),
                VideoFormat = null
            };
        }
        public void SetOS()
        {
            this.Ad.OS = new OS()
            {
                Name    = this.Parser.ParseOSName(),
                Version = this.Parser.ParseOSVersion()
            };
        }
        public void SetWLAN()
        {
            this.Ad.WLAN = this.Parser.ParseWLAN();
        }
        public void SetBluetooth()
        {
            this.Ad.Bluetooth = this.Parser.ParseBluetooth();
        }
        public void SetNFCChip()
        {
            this.Ad.HasNFCChip = this.Parser.ParseNFCChip();
        }        
        public void SetLineup()
        {
            this.Ad.Lineup = new PhoneLineup()
            {
                Name  = this.Parser.ParseLineupName(),
                Level = this.Parser.ParseLineupLevel()
            };
        }
        public void SetScreenPositionSensor()
        {
            this.Ad.HasScreenPositionSensor = this.Parser.ParseScreenPositionSensor();
        }
        public void SetLightSensor()
        {
            this.Ad.HasLightSensor = this.Parser.ParseLightSensor();
        }
        public void SetProximitySensor()
        {
            this.Ad.HasProximitySensor = this.Parser.ParseProximitySensor();
        }
        
        public override void SetCase()
        {
            this.Ad.Case              = this.Case;
            this.Ad.Case.Color        = this.Parser.ParseCaseColor();
            this.Ad.Case.Material     = this.Parser.ParseCaseMaterial();
            this.Ad.Case.IsShockproof = this.Parser.ParseCaseShockproof();            
        }
        public override void SetScreen()
        {
            this.Ad.Screen            = this.Screen;
            this.Ad.Screen.Diagonal   = this.Parser.ParseScreenDiagonal();
            this.Ad.Screen.Matrix     = this.Parser.ParseScreenMatrix();
            this.Ad.Screen.Resolution = this.Parser.ParseScreenResolution();
            this.Ad.Screen.PPI        = this.Parser.ParseScreenPPI();            
        }

        #endregion

    }
}
