﻿using ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Cameras
{
    public abstract class Camera
    {
        #region FIELDS

        private int     _id;
        private double? _resolution;
        private string  _description;
        private string  _videoFormat;        

        #endregion

        #region PROPERTIES

        public int Id
        {
            get => this._id;
            private set => this._id = value;
        }

        public double? Resolution
        {
            get => this._resolution;
            set
            {
                if (value.Less(0) && value.HasValue)
                {
                    throw new ArgumentException("Value must be greater than or equal to 0", nameof(this.Resolution));
                }

                this._resolution = value;
            }
        }
        public string  Description { get => this._description; set => this._description = value?.Trim(); }
        public string  VideoFormat { get => this._videoFormat; set => this._videoFormat = value?.Trim(); }

        #endregion

        #region CONSTRUCTORS       

        public Camera(double? resolution = null, string description = null, string videoFormat = null)
        {
            this.Resolution  = resolution;
            this.Description = description;
            this.VideoFormat = videoFormat;
        }

        #endregion
    }
}
