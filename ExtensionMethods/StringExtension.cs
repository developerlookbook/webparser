﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace ExtensionMethods
{
    public static class StringExtension
    {
        public static bool     IsNull           (this string value)
        {
            return (value == null);
        }
        public static bool     IsEmpty          (this string value)
        {
            return (value == String.Empty);
        }

        public static short?   ToNullableShort  (this string value)
        {
            return (short.TryParse(value?.Trim(), out short result) ? (short?)result : null);
        }                                                
        public static ushort?  ToNullableUshort (this string value)
        {
            return (ushort.TryParse(value?.Trim(), out ushort result) ? (ushort?)result : null);
        }                                                
        public static int?     ToNullableInt    (this string value)
        {
            return (int.TryParse(value?.Trim(), out int result) ? (int?)result : null);
        }                                                
        public static uint?    ToNullableUint   (this string value)
        {
            return (uint.TryParse(value?.Trim(), out uint result) ? (uint?)result : null);
        }                                                
        public static double?  ToNullableDouble (this string value)
        {
            if (value.IsNull()) return null;

            value = value?.Trim();
            value = Regex.Replace(value, @"\d+,\d+", (Match match) => Regex.Replace(match.ToString(), @",", @"."));

            var style   = NumberStyles.Number;
            var culture = CultureInfo.InvariantCulture;

            return (double.TryParse(value, style, culture, out double result) ? (double?)result : null);
        }
        public static decimal? ToNullableDecimal(this string value)
        {
            if (value.IsNull()) return null;

            value = value?.Trim();        
            value = Regex.Replace(value, @"\d+,\d+", (Match match) => Regex.Replace(match.ToString(), @",", @"."));

            var style   = NumberStyles.AllowDecimalPoint;
            var culture = CultureInfo.InvariantCulture;

            return (decimal.TryParse(value, style, culture, out decimal result) ? (decimal?)result : null);
        }
        public static ulong?   ToNullableUlong  (this string value)
        {
            return (ulong.TryParse(value?.Trim(), out ulong result) ? (ulong?)result : null);
        }
    }
}
