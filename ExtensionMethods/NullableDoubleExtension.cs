﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtensionMethods
{
    public static class NullableDoubleExtension
    {
        public static bool Less(this double? value, double comparable)
        {
            return (value < comparable);
        }

        public static bool Greater(this double? value, double comparable)
        {
            return (value > comparable);
        }
    }
}
