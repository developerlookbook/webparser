﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtensionMethods
{
    public static class IEnumerableExtension
    {
        public static bool IsNull<T>(this IEnumerable<T> value)
        {
            return (value == null);
        }

        public static void IfNull<T>(this IEnumerable<T> value, Action<IEnumerable<T>> action)
        {
            if (value.IsNull()) { action(value); }
        }
    }
}
