﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtensionMethods
{
    public static class NullableIntExtension
    {
        public static bool Less(this int? value, double comparable)
        {
            return (value < comparable);
        }

        public static bool Greater(this int? value, double comparable)
        {
            return (value > comparable);
        }
    }
}
