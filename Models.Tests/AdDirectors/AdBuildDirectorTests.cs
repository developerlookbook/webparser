﻿using HtmlAgilityPack;
using Models.AdBuilders;
using Models.AdDirectors;
using Models.Ads;
using MoyoAdParsers.AdParseres;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Models.Tests.AdDirectors
{
    class AdBuildDirectorTests
    {
        string PathToCellPhoneHtmlDocumentFile
        {
            get
            {
                string path;

                path = Assembly.GetExecutingAssembly().Location;
                path = Path.GetDirectoryName(path);
                path = $@"{path}\Data\CellPhone.html";

                return path;
            }
        }

        string PathToSmartPhoneHtmlDocumentFile
        {
            get
            {
                string path;

                path = Assembly.GetExecutingAssembly().Location;
                path = Path.GetDirectoryName(path);
                path = $@"{path}\Data\SmartPhone.html";

                return path;
            }
        }

        HtmlDocument CellPhoneHtmlDocument  { get; } = new HtmlDocument();
        HtmlDocument SmartPhoneHtmlDocument { get; } = new HtmlDocument();

        [SetUp]
        public void AdBuildDirectorTestsSetUp()
        {
            this.CellPhoneHtmlDocument.Load(this.PathToCellPhoneHtmlDocumentFile);
            this.SmartPhoneHtmlDocument.Load(this.PathToSmartPhoneHtmlDocumentFile);
        }
        
        [Test]
        public void Construct_ReturnsSmartPhoneAd()
        {            
            // Arrange
            var parser   = new SmartPhoneAdParser (this.SmartPhoneHtmlDocument);
            var builder  = new SmartPhoneAdBuilder(parser);
            var director = new AdBuildDirector<SmartPhoneAd>(builder);

            // Act
            var actual = director.Construct();

            // Assert
            Assert.That(actual.Barcode                  , Is.EqualTo(8806088366203));
            Assert.That(actual.Bluetooth                , Is.EqualTo("A2DP, v4.1" ));
            Assert.That(actual.BuiltInMemory            , Is.EqualTo(16));
            Assert.That(actual.Case.Color               , Is.EqualTo("золотой"));
            Assert.That(actual.Case.IsShockproof        , Is.EqualTo(false));
            Assert.That(actual.Case.Material            , Is.EqualTo("пластик"));
            Assert.That(actual.CommunicationStandard    , Is.EqualTo("3G, CDMA, EDGE, GPRS, GSM 850/1900, GSM 900/1800, HSDPA, WCDMA(UMTS) 900/2100"));
            Assert.That(actual.DataTransmissionStandards, Is.EqualTo("2G, 3G"));
            Assert.That(actual.Description              , Is.EqualTo("для девушек, для работы, с хорошей камерой"));
            Assert.That(actual.Dimension.Height         , Is.EqualTo(145.8));
            Assert.That(actual.Dimension.Thickness      , Is.EqualTo(8.1));
            Assert.That(actual.Dimension.Weight         , Is.EqualTo(159));
            Assert.That(actual.Dimension.Width          , Is.EqualTo(72.3d));
            Assert.That(actual.FrontCamera.Description  , Is.EqualTo(null));
            Assert.That(actual.FrontCamera.Resolution   , Is.EqualTo(5));
            Assert.That(actual.FrontCamera.VideoFormat  , Is.EqualTo(null));
            Assert.That(actual.GraphicsProcessor        , Is.EqualTo("Adreno 306"));
            Assert.That(actual.Guarantee                , Is.EqualTo(12));
            Assert.That(actual.HasLightSensor           , Is.EqualTo(false));
            Assert.That(actual.HasLTE                   , Is.EqualTo(false));
            Assert.That(actual.HasMemoryCardSupport     , Is.EqualTo(true));
            Assert.That(actual.HasNFCChip               , Is.EqualTo(false));
            Assert.That(actual.HasProximitySensor       , Is.EqualTo(true));
            Assert.That(actual.HasScreenPositionSensor  , Is.EqualTo(true));
            Assert.That(actual.Interfaces               , Is.EqualTo("3,5 mm, microUSB"));
            Assert.That(actual.Lineup.Level             , Is.EqualTo("J5"));
            Assert.That(actual.Lineup.Name              , Is.EqualTo("Galaxy"));
            Assert.That(actual.MadeIn                   , Is.EqualTo("Китай"));
            Assert.That(actual.MainCamera.Description   , Is.EqualTo("LED-вспышка, Автофокус"));
            Assert.That(actual.MainCamera.Resolution    , Is.EqualTo(13));
            Assert.That(actual.MainCamera.VideoFormat   , Is.EqualTo("FHD (1920x1080)"));
            Assert.That(actual.MaxMemoryCardCapacity    , Is.EqualTo("microSD до 128 Гб"));
            Assert.That(actual.Model                    , Is.EqualTo("Galaxy J5"));
            Assert.That(actual.OS.Name                  , Is.EqualTo("Android"));
            Assert.That(actual.OS.Version               , Is.EqualTo("Android 6.0"));
            Assert.That(actual.Photos.Count             , Is.EqualTo(10));
            Assert.That(actual.Price                    , Is.EqualTo(3_999));
            Assert.That(actual.Processor.ClockFrequency , Is.EqualTo(1.2));
            Assert.That(actual.Processor.CoresNumber    , Is.EqualTo(4));
            Assert.That(actual.Processor.Manufacturer   , Is.EqualTo("Qualcomm Snapdragon"));
            Assert.That(actual.Processor.Model          , Is.EqualTo("Snapdragon 410"));
            Assert.That(actual.RAM                      , Is.EqualTo(2));
            Assert.That(actual.SatelliteSystem          , Is.EqualTo("Glonass, GPS"));
            Assert.That(actual.Screen.Diagonal          , Is.EqualTo(5.2));
            Assert.That(actual.Screen.Matrix            , Is.EqualTo("Super AMOLED"));
            Assert.That(actual.Screen.PPI               , Is.EqualTo(282));
            Assert.That(actual.Screen.Resolution        , Is.EqualTo("1280x720 (HD)"));
            Assert.That(actual.SIMCard.Format           , Is.EqualTo("Micro-SIM"));
            Assert.That(actual.SIMCard.OperatingMode    , Is.EqualTo("попеременно"));
            Assert.That(actual.SIMCardsNumber           , Is.EqualTo(2));
            Assert.That(actual.Title                    , Is.EqualTo("Samsung Galaxy J5 2016 J510H/DS Gold"));
            Assert.That(actual.WirelessTechnologies     , Is.EqualTo("Bluetooth, GPS, Wi-Fi"));
            Assert.That(actual.WLAN                     , Is.EqualTo("802.11 a/b/g/n, Direct"));
            Assert.That(actual.YearOfIssue              , Is.EqualTo("2016"));
        }

        [Test]
        public void Construct_ReturnsCellPhoneAd()
        {
            // Arrange
            var parser   = new CellPhoneAdParser (this.CellPhoneHtmlDocument);
            var builder  = new CellPhoneAdBuilder(parser);
            var director = new AdBuildDirector<CellPhoneAd>(builder);

            // Act
            var actual = director.Construct();

            // Assert
            Assert.That(actual.Barcode                  , Is.EqualTo(6438158753358));
            Assert.That(actual.Case.Color               , Is.EqualTo("белый, серебристый"));
            Assert.That(actual.Case.Material            , Is.EqualTo("пластик/алюминий"));
            Assert.That(actual.CommunicationStandard    , Is.EqualTo("GSM 850/1900, GSM 900/1800"));
            Assert.That(actual.Description              , Is.EqualTo("недорогие"));
            Assert.That(actual.Dimension.Height         , Is.EqualTo(124.6));
            Assert.That(actual.Dimension.Thickness      , Is.EqualTo(10.9));
            Assert.That(actual.Dimension.Weight         , Is.EqualTo(91.8));
            Assert.That(actual.Dimension.Width          , Is.EqualTo(53.4));
            Assert.That(actual.Guarantee                , Is.EqualTo(12));
            Assert.That(actual.Interfaces               , Is.EqualTo("Audio-jack 3.5 mm, microUSB"));
            Assert.That(actual.MadeIn                   , Is.EqualTo("Китай"));
            Assert.That(actual.Photos.Count             , Is.EqualTo(3));
            Assert.That(actual.Price                    , Is.EqualTo(1_899));            
            Assert.That(actual.Screen.Diagonal          , Is.EqualTo(2.8));
            Assert.That(actual.Screen.Matrix            , Is.EqualTo("TFT"));
            Assert.That(actual.Screen.Resolution        , Is.EqualTo("320х240"));
            Assert.That(actual.SIMCard.Format           , Is.EqualTo("mini SIM"));
            Assert.That(actual.SIMCard.OperatingMode    , Is.EqualTo(null));
            Assert.That(actual.SIMCardsNumber           , Is.EqualTo(2));
            Assert.That(actual.Title                    , Is.EqualTo("Мобильный телефон Nokia 230 DS Silver White"));
            Assert.That(actual.WirelessTechnologies     , Is.EqualTo("Bluetooth 3.0"));
            Assert.That(actual.YearOfIssue              , Is.EqualTo("2015"));
        }
    }
}
